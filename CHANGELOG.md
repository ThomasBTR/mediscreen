# MEDISCEEN APP

## 2022-10-29 : Medisceen 3-0-0

- Add diabetes onto Patient entity and DTO
- api version 2.0.0


## 2022-10-24 : Medisceen 2-0-0

- Add connection to mediscreen-history to access medical reviews

## 2022-10-24 : Medisceen 1-0-0

- Get, Add and Update patient information 