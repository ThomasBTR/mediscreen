INSERT INTO T_PATIENT(id, first_name, last_name, birth_date, gender, address, phone_number, diabetes_assessment)
VALUES (0, 'Thomas', 'Robinson', '1992-10-09', 'MALE', '214 Roger Island St', '066-254-1452', 'NONE'),
       (1, 'Lucas', 'Ferguson', '1968-06-22', 'MALE', '2 Warren Street', '387-866-1399', 'NONE'),
       (2, 'Pippa', 'Rees', '1952-09-27', 'FEMALE', '745 West Valley Farms Drive', '628-423-0993', 'NONE'),
       (3, 'Edward', 'Arnold', '1952-11-11', 'MALE', '599 East Garden Ave', '123-727-2779', 'NONE'),
       (4, 'Anthony', 'Sharp', '1946-11-26', 'MALE', '894 Hall Street', '451-761-8383', 'NONE'),
       (5, 'Wendy', 'Ince', '1958-06-29', 'FEMALE', '4 Southampton Road', '802-911-9975', 'NONE'),
       (6, 'Tracey', 'Ross', '1949-12-07', 'FEMALE', '40 Sulphur Springs Dr', '131-396-5049', 'NONE'),
       (7, 'Claire', 'Wilson', '1966-12-31', 'FEMALE', '12 Cobblestone St', '300-452-1091', 'NONE'),
       (8, 'Max', 'Buckland', '1945-06-24', 'MALE', '193 Vale St', '833-534-0864', 'NONE'),
       (9, 'Natalie', 'Clark', '1964-06-18', 'FEMALE', '12 Beechwood Road', '241-467-9197', 'NONE'),
       (10, 'Piers', 'Bailey', '1959-06-28', 'MALE', '1202 Bumble Dr', '747-815-0557', 'NONE');

insert into T_USER(id, username, password, role, enabled)
values (0, 'admin', '$2a$10$49j0YdKIsPPdpn3u.bBRuOhZt1Ldboiq.4lP5jx9sPLIyL43N5aa6', 'ROLE_ADMIN', 1),
       (1, 'user', '$2a$10$jN0HquqlLgEPNtN4k6Uccu.gGKexFdH7MJe4M/dbtiYnAs5e.Xvxu', 'ROLE_USER', 1);
