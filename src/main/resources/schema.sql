CREATE TABLE T_PATIENT
(
    id           INT NOT NULL AUTO_INCREMENT,
    first_name   VARCHAR(255) NOT NULL,
    last_name    VARCHAR(255) NOT NULL,
    birth_date   VARCHAR(50)  NOT NULL,
    gender       ENUM('MALE', 'FEMALE') DEFAULT 'MALE',
    address      VARCHAR(255),
    phone_number VARCHAR(15),
    diabetes_assessment ENUM('NONE','BORDERLINE','IN DANGER','EARLY ONSET') DEFAULT 'NONE',

    PRIMARY KEY (id)
);

CREATE TABLE T_USER
(
    id       INT     NOT NULL AUTO_INCREMENT,
    username VARCHAR(125),
    password VARCHAR(125),
    role VARCHAR(125),
    enabled  TINYINT NOT NULL DEFAULT 1,

    PRIMARY KEY (id)
);