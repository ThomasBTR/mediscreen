package com.abernathyclinic.mediscreen.exceptions;

public class MediscreenRuntimeException extends RuntimeException{

    public MediscreenRuntimeException(String s) {
        super(s);
    }

    public MediscreenRuntimeException(final String s, final Throwable throwable) {
        super(s, throwable);
    }

}
