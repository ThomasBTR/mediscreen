package com.abernathyclinic.mediscreen.services;

import com.abernathyclinic.mediscreen.entities.Patient;
import com.abernathyclinic.mediscreen.exceptions.MediscreenRuntimeException;
import com.abernathyclinic.mediscreen.models.PatientDTO;
import com.abernathyclinic.mediscreen.repositories.IPatientRepository;
import com.abernathyclinic.mediscreen.utils.IPatientToDtoMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
public class PatientService {

    private static final Logger LOGGER = LoggerFactory.getLogger(PatientService.class);

    private final IPatientRepository patientRepository;

    private String logMessage;

    public PatientService(IPatientRepository patientRepository) {
        this.patientRepository = patientRepository;
    }

    @Transactional
    public PatientDTO getPatient(Integer patientId) {
        LOGGER.debug("Patient with id {} is being gathered", patientId);
        return IPatientToDtoMapper.INSTANCE.patientToDTO(patientRepository.getReferenceById(patientId));
    }

    @Transactional
    public PatientDTO getPatient(String firstName, String lastName) {
        LOGGER.debug("Patient with first name {} and last name {} is being gathered", firstName, lastName);
        return IPatientToDtoMapper.INSTANCE.patientToDTO(patientRepository.getPatientByFirstNameAndLastName(firstName, lastName));
    }

    @Transactional
    public PatientDTO addPatient(PatientDTO patientDtoToAdd) {
        PatientDTO addedPatientDto;
        try {
            LOGGER.debug("Started to add {} {} to the database", patientDtoToAdd.getFirstName(), patientDtoToAdd.getLastName());
            Patient addPatient = IPatientToDtoMapper.INSTANCE.patientDTOtoPatient(patientDtoToAdd);
            addPatient.setId(getNextId());
            addPatient = patientRepository.save(addPatient);
            addedPatientDto = IPatientToDtoMapper.INSTANCE.patientToDTO(addPatient);
        } catch (MediscreenRuntimeException e) {
            logMessage = String.format("%s %s %s", "Error while adding to database patient", patientDtoToAdd.getFirstName(),
                    patientDtoToAdd.getLastName());
            LOGGER.error(logMessage);
            throw new MediscreenRuntimeException(logMessage, e);
        }
        return addedPatientDto;
    }

    @Transactional
    public Integer getNextId() {
        return patientRepository.findAll().size();
    }

    public PatientDTO updatePatient(PatientDTO patientDtoToUpdate) {
        PatientDTO updatedPatientDto;
        try {
            LOGGER.debug("Started to update {} {} to the database", patientDtoToUpdate.getFirstName(), patientDtoToUpdate.getLastName());
            Patient updatablePatient = IPatientToDtoMapper.INSTANCE.patientDTOtoPatient(patientDtoToUpdate);
            updatablePatient = patientRepository.save(updatablePatient);
            updatedPatientDto = IPatientToDtoMapper.INSTANCE.patientToDTO(updatablePatient);
        } catch (MediscreenRuntimeException e) {
            logMessage = String.format("%s %s %s", "Error while updating to database patient", patientDtoToUpdate.getFirstName(),
                    patientDtoToUpdate.getLastName());
            LOGGER.error(logMessage);
            throw new MediscreenRuntimeException(logMessage, e);
        }
        return updatedPatientDto;

    }


}
