package com.abernathyclinic.mediscreen.services;

import com.abernathyclinic.mediscreen.configurations.securities.SpringSecurityConfig;
import com.abernathyclinic.mediscreen.entities.User;
import com.abernathyclinic.mediscreen.repositories.IUserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
public class UserServices {

    /**
     * The User repository.
     */
    private final IUserRepository userRepository;

    private final SpringSecurityConfig springSecurityConfig;

    /**
     * The constant LOGGER.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(UserServices.class);

    /**
     * Instantiates a new User services.
     *
     * @param userRepository       the user repository
     * @param springSecurityConfig the spring security config
     */
    public UserServices(final IUserRepository userRepository, SpringSecurityConfig springSecurityConfig) {
        this.userRepository = userRepository;
        this.springSecurityConfig = springSecurityConfig;
    }

    /**
     * Gets next id.
     *
     * @return the next id
     */
    @Transactional
    public int getNextId() {
        return userRepository.findAll().size();
    }

    /**
     * Create user.
     *
     * @param username the username
     * @param password the password
     */
    @Transactional
    public void createUser(final String username, final String password) {
        User user;
        List<User> verify = userRepository.findAll();
        if (verify.stream().anyMatch(user1 -> user1.getUsername().equals(username))) {
            LOGGER.warn("user already in database with username {}", username);
            //throw exception.
        } else {
            user = new User(getNextId(), username, springSecurityConfig.passwordEncoder().encode(password));
            userRepository.save(user);
        }
    }
}