package com.abernathyclinic.mediscreen.entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import java.time.Instant;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity(name = "T_PATIENT")
public class Patient {

    @Id
    @Column(name = "id")
    private Integer id;

    @NotNull
    @Column(name = "first_name")
    private String firstName;

    @NotNull
    @Column(name = "last_name")
    private String lastName;

    @Column(name = "birth_date")
    private Instant birthDate;

    @Column(name = "gender")
    @Enumerated(EnumType.STRING)
    private Gender gender;

    @Column(name = "address")
    private String address;

    @Column(name = "phone_number")
    private String phoneNumber;

    @Column(name = "diabetes_assessment")
    @Enumerated(EnumType.STRING)
    private EDiabetesAssessment diabetesAssessment;

    public Patient(String firstName, String lastName, Instant birthDate, String gender, String address,
                   String phoneNumber) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.birthDate = birthDate;
        this.gender = Gender.valueOf(gender);
        this.address = address;
        this.phoneNumber = phoneNumber;
        this.diabetesAssessment = EDiabetesAssessment.NONE;
    }
}
