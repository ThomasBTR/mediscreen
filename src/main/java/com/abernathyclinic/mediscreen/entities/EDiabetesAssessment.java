package com.abernathyclinic.mediscreen.entities;

public enum EDiabetesAssessment {

    NONE("NONE"),
    BORDERLINE("BORDERLINE"),
    IN_DANGER("IN DANGER"),
    EARLY_ONSET("EARLY ONSET");

    private final String value;

    EDiabetesAssessment(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
