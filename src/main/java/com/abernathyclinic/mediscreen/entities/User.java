package com.abernathyclinic.mediscreen.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * The type User.
 */
@Entity
@Table(name = "T_USER")
public class User {

    /**
     * The Id.
     */
    @Id
    @NotNull
    @Column(name = "id")
    private Integer id;

    /**
     * The Username.
     */
    @NotBlank(message = "Username is mandatory")
    private String username;

    /**
     * The Password.
     */
    @NotBlank(message = "Password is mandatory")
    private String password;

    /**
     * The Role.
     */
    @NotBlank(message = "Role is mandatory")
    private String role;

    /**
     * The Enabled.
     */
    @Column(name = "enabled", columnDefinition = "integer not null default 1")
    private int enabled;

    /**
     * Instantiates a new User.
     */
    public User() {

    }

    /**
     * Instantiates a new User.
     *
     * @param id       the id
     * @param username the username
     * @param password the password
     */
    public User(final int id, final String username, final String password) {
        this.username = username;
        this.password = password;
        this.role = "ROLE_USER";
        this.id = id;
        this.enabled = 1;
    }

    /**
     * Gets id.
     *
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * Sets id.
     *
     * @param id the id
     */
    public void setId(final Integer id) {
        this.id = id;
    }

    /**
     * Gets username.
     *
     * @return the username
     */
    public String getUsername() {
        return username;
    }

    /**
     * Sets username.
     *
     * @param username the username
     */
    public void setUsername(final String username) {
        this.username = username;
    }

    /**
     * Gets password.
     *
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * Sets password.
     *
     * @param password the password
     */
    public void setPassword(final String password) {
        this.password = password;
    }

    /**
     * Gets role.
     *
     * @return the role
     */
    public String getRole() {
        return role;
    }

    /**
     * Sets role.
     *
     * @param role the role
     */
    public void setRole(final String role) {
        this.role = role;
    }

    /**
     * Gets enabled.
     *
     * @return the enabled
     */
    public int getEnabled() {
        return enabled;
    }

    /**
     * Sets enabled.
     *
     * @param enabled the enabled
     */
    public void setEnabled(int enabled) {
        this.enabled = enabled;
    }
}
