package com.abernathyclinic.mediscreen.proxies;


import com.abernathyclinic.mediscreen.beans.MedicalReviewDtoBean;
import io.swagger.v3.oas.annotations.Parameter;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

import javax.validation.Valid;
import java.util.List;

@FeignClient(name = "mediscreen-history")
public interface MicroserviceMediscreenHistoryProxy {


    @GetMapping(value = "/api/v1/medicalReviews/{patientId}")
    List<MedicalReviewDtoBean> getMedicalReviewsByPatientId(
            @Parameter(name = "patientId", description = "ID of patient to return", required = true) @PathVariable("patientId") Integer patientId
    );

    @GetMapping(value = "/api/v1/medicalReviews/{patientId}/{noteId}")
    MedicalReviewDtoBean getMedicalReviewByNoteId(
            @Parameter(name = "patientId", description = "ID of the patient", required = true) @PathVariable("patientId") Integer patientId,
            @Parameter(name = "noteId", description = "ID of the medical review to return", required = true) @PathVariable("noteId") String noteId
    );

    @PutMapping(value = "/api/v1/medicalReviews/{patientId}")
    MedicalReviewDtoBean updateMedicalReviewByPatientId(
            @Parameter(name = "patientId", description = "ID of patient to return", required = true) @PathVariable("patientId") Integer patientId,
            @Parameter(name = "MedicalReviewDTO", description = "update an existing patient in database", required = true) @Valid @RequestBody MedicalReviewDtoBean medicalReviewDTO
    );

    @PostMapping(value = "/api/v1/medicalReviews/{patientId}")
    MedicalReviewDtoBean addMedicalReviewByPatientId(
            @Parameter(name = "patientId", description = "ID of patient to return", required = true) @PathVariable("patientId") Integer patientId,
            @Parameter(name = "MedicalReviewDTO", description = "insert a new medicalReview in database", required = true) @Valid @RequestBody MedicalReviewDtoBean medicalReviewDTO
    );
}
