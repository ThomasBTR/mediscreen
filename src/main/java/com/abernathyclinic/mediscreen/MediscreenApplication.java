package com.abernathyclinic.mediscreen;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

@EnableFeignClients("com.abernathyclinic.mediscreen")
@SpringBootApplication
@EnableDiscoveryClient
public class MediscreenApplication {

    public static void main(String[] args) {
        SpringApplication.run(MediscreenApplication.class, args);
    }
}
