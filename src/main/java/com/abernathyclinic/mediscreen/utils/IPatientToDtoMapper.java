package com.abernathyclinic.mediscreen.utils;

import com.abernathyclinic.mediscreen.entities.Patient;
import com.abernathyclinic.mediscreen.models.PatientDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper
public interface IPatientToDtoMapper {

    IPatientToDtoMapper INSTANCE = Mappers.getMapper(IPatientToDtoMapper.class);

    Patient patientDTOtoPatient(PatientDTO patientDTO);


    PatientDTO patientToDTO(Patient patient);
}
