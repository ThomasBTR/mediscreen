package com.abernathyclinic.mediscreen.configurations.jacksons;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import lombok.Data;

import java.time.Instant;
import java.time.format.DateTimeFormatter;

/**
 * The type Jackson object mapper.
 */
@Data
public class JacksonObjectMapper extends ObjectMapper {

    /**
     * The Object mapper.
     */
    private ObjectMapper objectMapper;

    /**
     * Instantiates a new Jackson object mapper.
     */
    public JacksonObjectMapper() {
        final ObjectMapper mapper = new ObjectMapper();
        mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES)
                .disable(SerializationFeature.WRAP_ROOT_VALUE)
                .disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
        final JavaTimeModule javaTimeModule = new JavaTimeModule();
        javaTimeModule.addSerializer(Instant.class, new CustomInstantSerializer(DateTimeFormatter.ISO_INSTANT));
        javaTimeModule.addDeserializer(Instant.class, new CustomInstantDeserializer());
        mapper.registerModule(javaTimeModule);
        this.objectMapper = mapper;
    }


}
