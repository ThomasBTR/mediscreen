package com.abernathyclinic.mediscreen.configurations.securities;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;

import javax.sql.DataSource;

/**
 * The type Spring security config.
 */
@Configuration
@EnableWebSecurity
@RefreshScope
public class SpringSecurityConfig {

    /**
     * The Data source.
     */
    private final DataSource dataSource;

    /**
     * The Custom oidc user service.
     */
    private final CustomOidcUserService customOidcUserService;

    /**
     * The Users query.
     */
    @Value("${mediscreen.queries.users}")
    private String usersQuery;

    /**
     * The Roles query.
     */
    @Value("${mediscreen.queries.roles}")
    private String rolesQuery;

    /**
     * Instantiates a new Spring security config.
     *
     * @param dataSource            the data source
     * @param customOidcUserService oidcUserService from user
     */
    public SpringSecurityConfig(final DataSource dataSource, final CustomOidcUserService customOidcUserService) {
        this.dataSource = dataSource;
        this.customOidcUserService = customOidcUserService;
    }

    /**
     * .
     *
     * @param auth the auth
     * @throws Exception the exception
     */
    @Autowired
    public void configureGlobal(final AuthenticationManagerBuilder auth) throws Exception {
        auth.jdbcAuthentication()
                .dataSource(dataSource)
                .usersByUsernameQuery(usersQuery)
                .authoritiesByUsernameQuery(rolesQuery)
                .passwordEncoder(new BCryptPasswordEncoder());

    }

    /**
     * .
     *
     * @param httpSecurity the http
     * @throws Exception the exception
     */
    @Bean
    public SecurityFilterChain filterChain(HttpSecurity httpSecurity)
            throws Exception {
        String login = "/login";
        httpSecurity.authorizeRequests().antMatchers("/api/v1/**").permitAll();
        httpSecurity.authorizeRequests()
                .antMatchers(
                        "/",
                        "/favicon.ico",
                        "/webjars/",
                        "/js/",
                        "/img/",
                        "/css/",
                        login,
                        "/home"
                ).permitAll()
                .antMatchers("/patient/**").hasAnyRole("USER", "ADMIN")
                .and()
                .formLogin()
                .loginPage(login)
                .failureUrl("/login-error")
                .defaultSuccessUrl("/patient/info")
                .and()
                .oauth2Login().userInfoEndpoint().oidcUserService(customOidcUserService)
                .and()
                .loginPage(login)
                .failureUrl("/login-error")
                .defaultSuccessUrl("/patient/info")
                .and()
                .logout().deleteCookies("JSESSIONID")
                .logoutSuccessUrl(login);


        httpSecurity.csrf()
                .ignoringAntMatchers("/h2-console/**","/api/v1/**");
        httpSecurity.headers()
                .frameOptions()
                .sameOrigin();
        return httpSecurity.build();
    }

    /**
     * Password encoder password encoder.
     *
     * @return the password encoder
     */
    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

}
