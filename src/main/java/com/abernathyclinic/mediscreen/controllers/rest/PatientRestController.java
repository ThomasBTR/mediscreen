package com.abernathyclinic.mediscreen.controllers.rest;

import com.abernathyclinic.mediscreen.api.PatientApi;
import com.abernathyclinic.mediscreen.exceptions.MediscreenRuntimeException;
import com.abernathyclinic.mediscreen.models.PatientDTO;
import com.abernathyclinic.mediscreen.services.PatientService;
import io.swagger.v3.oas.annotations.Parameter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@RestController
public class PatientRestController implements PatientApi {

    private final PatientService patientService;

    private static final Logger LOGGER = LoggerFactory.getLogger(PatientRestController.class);

    public PatientRestController(PatientService patientService) {
        this.patientService = patientService;
    }

    @Override
    public ResponseEntity<PatientDTO> getPatientById(
            @Parameter(name = "patientId", description = "ID of pet to return", required = true) @PathVariable("patientId") Integer patientId
    ) {
        ResponseEntity<PatientDTO> responseEntity = null;
        try{
            responseEntity = ResponseEntity.ok(patientService.getPatient(patientId));
            LOGGER.info("Gathered info for patient with id {}", patientId);
        }catch (MediscreenRuntimeException exception) {
            LOGGER.error("Error while gathering information for patient id#{}", patientId, exception);
        }
        return responseEntity;
    }

    @Override
    public ResponseEntity<PatientDTO> getPatientByFirstAndLastName(
            @NotNull @Parameter(name = "firstName", description = "Patient first name", required = true) @Valid @RequestParam(value = "firstName", required = true) String firstName,
            @NotNull @Parameter(name = "lastName", description = "Patient last name", required = true) @Valid @RequestParam(value = "lastName", required = true) String lastName
    ) {
        ResponseEntity<PatientDTO> patientDTOGathered = null;
        try{
            patientDTOGathered = ResponseEntity.ok(patientService.getPatient(firstName, lastName));
            LOGGER.info("{} {} information as been gathered in database with id# {}",patientDTOGathered.getBody().getFirstName(), patientDTOGathered.getBody().getLastName(), patientDTOGathered.getBody().getId());
        } catch (MediscreenRuntimeException e) {
            LOGGER.error("Error while getting patient {} {} in database",firstName,lastName,e);
        }
        return patientDTOGathered;
    }

    @Override
    public ResponseEntity<PatientDTO> addPatient(
            @Parameter(name = "PatientDTO", description = "create a new patient in database") @Valid @RequestBody(required = false) PatientDTO patientDTO
    ) {
        ResponseEntity<PatientDTO> addPatientResponseEntity = null;
        try{
            addPatientResponseEntity = ResponseEntity.ok(patientService.addPatient(patientDTO));
            LOGGER.info("{} {} information as been added in database with id# {}",patientDTO.getFirstName(), patientDTO.getLastName(), addPatientResponseEntity.getBody().getId());
        } catch (MediscreenRuntimeException e) {
            LOGGER.error("Error while adding patient in database",e);
        }
        return addPatientResponseEntity;
    }

    @Override
    public ResponseEntity<PatientDTO> updatePatient(
            @Parameter(name = "PatientDTO", description = "update an existing patient in database", required = true) @Valid @RequestBody PatientDTO patientDTO
    ) {
        ResponseEntity<PatientDTO> updatePatientResponseEntity = null;
        try{
            updatePatientResponseEntity = ResponseEntity.ok(patientService.updatePatient(patientDTO));
            LOGGER.info("Patient {} {} as been updated in database", patientDTO.getFirstName(), patientDTO.getLastName());
        } catch (MediscreenRuntimeException e) {
            LOGGER.error("Error while updating patient", e);
        }
        return updatePatientResponseEntity;
    }
}
