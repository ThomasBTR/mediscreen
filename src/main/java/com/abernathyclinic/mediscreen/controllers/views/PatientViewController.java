package com.abernathyclinic.mediscreen.controllers.views;

import com.abernathyclinic.mediscreen.beans.MedicalReviewDtoBean;
import com.abernathyclinic.mediscreen.controllers.rest.PatientRestController;
import com.abernathyclinic.mediscreen.exceptions.MediscreenRuntimeException;
import com.abernathyclinic.mediscreen.models.PatientDTO;
import com.abernathyclinic.mediscreen.proxies.MicroserviceMediscreenHistoryProxy;
import com.abernathyclinic.mediscreen.services.PatientService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;
import javax.ws.rs.Path;
import java.time.Instant;
import java.util.List;
import java.util.Objects;

@Controller
@RequestMapping(path = "/patient")
public class PatientViewController {

    private final PatientRestController patientRestController;

    private final PatientService patientService;
    
    private static final String PATIENT_DTO = "patientDTO";

    private static final String DATA = "patient/data";

    private static final Logger LOGGER = LoggerFactory.getLogger(PatientViewController.class);

    private final MicroserviceMediscreenHistoryProxy microserviceMediscreenHistoryProxy;


    public PatientViewController(PatientRestController patientRestController, PatientService patientService, MicroserviceMediscreenHistoryProxy microserviceMediscreenHistoryProxy) {
        this.patientRestController = patientRestController;
        this.patientService = patientService;
        this.microserviceMediscreenHistoryProxy = microserviceMediscreenHistoryProxy;
    }

    /**
     * Home string.
     *
     * @param model the model
     * @return the string
     */
    @GetMapping("/info")
    public String getInfoPanel(final PatientDTO patientDTO, final Model model) {
        model.addAttribute(PATIENT_DTO, patientDTO);
        LOGGER.info("Info Panel to research for a patient");
        return "patient/info";
    }

    public void enrichPatientData(final ResponseEntity<PatientDTO> responseEntity, final Model model) {
        model.addAttribute(PATIENT_DTO, responseEntity.getBody());
        model.addAttribute("id", Objects.requireNonNull(responseEntity.getBody()).getId().toString());
        List<MedicalReviewDtoBean> medicalReviewDtoBeans = microserviceMediscreenHistoryProxy.getMedicalReviewsByPatientId(Objects.requireNonNull(responseEntity.getBody().getId()));
        model.addAttribute("medicalReviews", medicalReviewDtoBeans);
        LOGGER.info("{} {} data gathered from medical review database", Objects.requireNonNull(responseEntity.getBody().getFirstName()), Objects.requireNonNull(responseEntity.getBody().getLastName()));
    }

    @PostMapping("/info/search")
    public String getPatientFromFirstAndLastName(@ModelAttribute("patientDTO") @Valid final PatientDTO patientDTO, final BindingResult bindingResult, final Model model) {
        String getPatientFromFirstAndLastName;
        //1. Return error if PatientDTO contain invalid data
        if (bindingResult.hasErrors()) {
            getPatientFromFirstAndLastName = getInfoPanel(patientDTO, model);
        } else {
            //2. Get and return data from database
            ResponseEntity<PatientDTO> responseEntity = patientRestController.getPatientByFirstAndLastName(patientDTO.getFirstName(), patientDTO.getLastName());
            LOGGER.info("{} {} gathered from database using first and last name", patientDTO.getFirstName(), patientDTO.getLastName());
            enrichPatientData(responseEntity, model);
            getPatientFromFirstAndLastName = DATA;
        }
        return getPatientFromFirstAndLastName;
    }

    @PostMapping("/update/{id}")
    public String updatePatient(@PathVariable("id")final Integer id, @ModelAttribute("patientDTO")@Valid final PatientDTO patientDTO, final BindingResult bindingResult, final Model model) {
        String updatePatient;
        //Return error if PatientDTO contain invalid data
        if (bindingResult.hasErrors()) {
            updatePatient = getInfoPanel(patientDTO, model);
        } else {
            //Else get data from database and return form
            ResponseEntity<PatientDTO> responseEntity = patientRestController.updatePatient(patientDTO);
            LOGGER.info("{} {} updated in database", patientDTO.getFirstName(), patientDTO.getLastName());
            model.addAttribute(PATIENT_DTO, responseEntity.getBody());
            updatePatient = getPatientFromFirstAndLastName(responseEntity.getBody(), bindingResult, model);
        }
        return updatePatient;
    }

    @GetMapping("add")
    public String addPatient(final PatientDTO patientDTO, final Model model) {
        patientDTO.setId(patientService.getNextId());
        model.addAttribute(PATIENT_DTO, patientDTO);
        return "patient/add";
    }

    @PostMapping("/validate")
    public String validate(@Valid final PatientDTO patientDTO, final BindingResult bindingResult, final Model model) {
        String validateNewPatient;
        //Return error if CurvePointDTO contain invalid data
        if (bindingResult.hasErrors()) {
            validateNewPatient = "patient/add";
        } else {
            // Else save and return the CurvePointDTO added
            ResponseEntity<PatientDTO> responseEntity = patientRestController.addPatient(patientDTO);
            LOGGER.info("Patient {} {} added to the database", patientDTO.getFirstName(), patientDTO.getLastName());
            model.addAttribute(PATIENT_DTO, responseEntity.getBody());
            model.addAttribute("id", Objects.requireNonNull(responseEntity.getBody()).getId().toString());
            validateNewPatient = DATA;
        }
        return validateNewPatient;
    }

    @GetMapping("addReview/{id}")
    public String addReviewGetPage(@PathVariable("id")final Integer id, @ModelAttribute("patientDTO")@Valid final PatientDTO patientDTO, final Model model) {
        MedicalReviewDtoBean medicalReview = new MedicalReviewDtoBean();
        medicalReview.setPatientId(id);
        medicalReview.setDate(Instant.now());
        model.addAttribute("medicalReview", medicalReview);
        return "patient/addReview";
    }

    @PostMapping("addReview/{id}")
    public String addReviewPerform(@PathVariable("id")final Integer id, @ModelAttribute("medicalReview")@Valid final MedicalReviewDtoBean medicalReview, final Model model) {
        try{
            MedicalReviewDtoBean medicalReviewDtoBean = microserviceMediscreenHistoryProxy.addMedicalReviewByPatientId(id, medicalReview);
            LOGGER.info("Note for patient id # {} created in medical review database", medicalReviewDtoBean.getPatientId());
            getPatientByIdAndEnrichData(id, model);
        } catch (MediscreenRuntimeException exception) {
            LOGGER.error("Error with medical-history server while adding medical review", exception);
        }
        return DATA;
    }

    @GetMapping("editReview/{patientId}/{noteId}")
    public String editReviewGetPage(@PathVariable("patientId")final Integer patienId, @PathVariable("noteId")final String noteId, @ModelAttribute("medicalReview") MedicalReviewDtoBean medicalReview, final Model model) {
        try{
            MedicalReviewDtoBean medicalReviewToUpdate = microserviceMediscreenHistoryProxy.getMedicalReviewByNoteId(patienId, noteId);
            LOGGER.info("Request editing page regarding medical review of patient id # {} from date {}", noteId, medicalReviewToUpdate.getDate());
            model.addAttribute("medicalReview", medicalReviewToUpdate);
        } catch (MediscreenRuntimeException exception) {
            LOGGER.error("Error with medical-history server while adding medical review", exception);
        }
        return "patient/editReview";
    }

    @PostMapping("editReview/{patientId}/{noteId}")
    public String editReviewPerform(@PathVariable("patientId")final Integer patientId, @PathVariable("noteId")final String noteId, @ModelAttribute("medicalReview") final MedicalReviewDtoBean medicalReview, final Model model) {
        try {
            MedicalReviewDtoBean updateMedicalReview = microserviceMediscreenHistoryProxy.updateMedicalReviewByPatientId(patientId,medicalReview);
            LOGGER.info("Note from patient id# {} has been updated", updateMedicalReview.getPatientId());
             getPatientByIdAndEnrichData(patientId, model);
        } catch (MediscreenRuntimeException exception) {
            LOGGER.error("Error with medical-history server while updating medical review", exception);
        }
        return DATA;
    }

    public void getPatientByIdAndEnrichData(final Integer id, final Model model) {
        ResponseEntity<PatientDTO> responseEntity = patientRestController.getPatientById(id);
        LOGGER.info("{} {} gathered from database using id", Objects.requireNonNull(responseEntity.getBody()).getFirstName(),Objects.requireNonNull(responseEntity.getBody()).getLastName());
        enrichPatientData(responseEntity, model);
    }

}
