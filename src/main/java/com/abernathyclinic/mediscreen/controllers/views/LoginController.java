package com.abernathyclinic.mediscreen.controllers.views;

import com.abernathyclinic.mediscreen.models.UserDTO;
import com.abernathyclinic.mediscreen.repositories.IUserRepository;
import com.abernathyclinic.mediscreen.services.UserServices;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.ResolvableType;
import org.springframework.security.oauth2.client.registration.ClientRegistration;
import org.springframework.security.oauth2.client.registration.ClientRegistrationRepository;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.Map;

/**
 * The type Login controller.
 */
@Controller
public class LoginController {

    /**
     * The constant LOGGER.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(LoginController.class);

    /**
     * The constant OAUTH_2_AUTHORIZATION.
     */
    private static final String OAUTH_2_AUTHORIZATION = "oauth2/authorization";

    /**
     * The Oauth 2 authentication urls.
     */
    private final Map<String, String> oauth2AuthenticationUrls = new HashMap<>();

    /**
     * The Client registration repository.
     */
    private final ClientRegistrationRepository clientRegistrationRepository;

    private final IUserRepository userRepository;

    private final UserServices userServices;

    /**
     * Instantiates a new Login controller.
     *
     * @param clientRegistrationRepository the client registration repository
     * @param userRepository               the user repository
     * @param userServices                 the user services
     */
    public LoginController(ClientRegistrationRepository clientRegistrationRepository, IUserRepository userRepository, UserServices userServices) {
        this.clientRegistrationRepository = clientRegistrationRepository;
        this.userRepository = userRepository;
        this.userServices = userServices;
    }

    @GetMapping("/")
    public String index(final Model model){
        return "login";
    }

    /**
     * Login string.
     *
     * @param model the model
     * @return the string
     */
    @GetMapping("/login")
    public String login(final Model model) {
        Iterable<ClientRegistration> clientRegistrations = null;
        ResolvableType type = ResolvableType.forInstance(clientRegistrationRepository)
                .as(Iterable.class);
        if (type != ResolvableType.NONE
                &&
                ClientRegistration.class.isAssignableFrom(type.resolveGenerics()[0])) {
            clientRegistrations = (Iterable<ClientRegistration>) clientRegistrationRepository;
        }

        assert clientRegistrations != null;
        clientRegistrations.forEach(registration ->
                oauth2AuthenticationUrls.put(registration.getClientName(),
                        OAUTH_2_AUTHORIZATION + "/" + registration.getRegistrationId()));
        model.addAttribute("urls", oauth2AuthenticationUrls);
        return "login";
    }

    /**
     * Gets all user articles.
     *
     * @return the all user articles
     */
    @GetMapping("secure/article-details")
    public ModelAndView getAllUserArticles() {
        ModelAndView mav = new ModelAndView();
        mav.addObject("users", userRepository.findAll());
        mav.setViewName("user/list");
        return mav;
    }

    /**
     * Login error string.
     *
     * @param model the model
     * @return the string
     */
    // Login form with error
    @GetMapping("/login-error")
    public String loginError(final Model model) {
        model.addAttribute("loginError", true);
        return "login";
    }

    /**
     * Gets signup.
     *
     * @param userDTO the user dto
     * @param model   the model
     * @return the signup
     */
    //Signup form
    @GetMapping("/signup")
    public String getSignup(final UserDTO userDTO, final Model model) {
        userDTO.setId(userServices.getNextId());
        model.addAttribute("userDTO", userDTO);
        return "signup";
    }

    /**
     * Post signup string.
     *
     * @param userDTO       the user dto
     * @param bindingResult the binding result
     * @param model         the model
     * @return the string
     */
    @PostMapping("/signup")
    public String postSignup(@Valid @ModelAttribute("userDTO") final UserDTO userDTO, final BindingResult bindingResult, final Model model) {
        String postSignUpResponse;
        //Return error if UserDTO contain invalid data
        if (bindingResult.hasErrors()) {
            postSignUpResponse = "signup";
        } else {
            userServices.createUser(userDTO.getUsername(), userDTO.getPassword());
            LOGGER.info("User {} added to the database", userDTO.getUsername());
            model.addAttribute("signUp", true);
            postSignUpResponse = login(model);
        }
        return postSignUpResponse;
    }

}
