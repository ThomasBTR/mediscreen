package com.abernathyclinic.mediscreen.beans;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.Instant;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class MedicalReviewDtoBean {

    private Integer patientId;
    private String noteId;
    private String notes;
    private Instant date;

}
