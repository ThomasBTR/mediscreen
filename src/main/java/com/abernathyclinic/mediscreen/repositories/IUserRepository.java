package com.abernathyclinic.mediscreen.repositories;

import com.abernathyclinic.mediscreen.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface IUserRepository extends JpaRepository<User, Integer> {
    /**
     * Find user by username user.
     *
     * @param username the username
     * @return the user
     */
    @Query("SELECT user FROM User user WHERE user.username LIKE %:variable%")
    User findUserByUsername(@Param("variable")String username);


}
