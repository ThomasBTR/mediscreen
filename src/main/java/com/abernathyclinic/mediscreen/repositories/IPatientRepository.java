package com.abernathyclinic.mediscreen.repositories;

import com.abernathyclinic.mediscreen.entities.Patient;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface IPatientRepository extends JpaRepository<Patient, Integer> {

    @Query("SELECT patient FROM T_PATIENT patient WHERE patient.firstName LIKE %:firstName% AND patient.lastName LIKE %:lastName%")
    Patient getPatientByFirstNameAndLastName(@Param("firstName") String firstName, @Param("lastName") String lastName);

}
