package com.abernathyclinic.mediscreen.controllers;

import com.abernathyclinic.mediscreen.configurations.jacksons.JacksonConfiguration;
import com.abernathyclinic.mediscreen.models.PatientDTO;
import com.abernathyclinic.mediscreen.utils.UTHelper;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Profile;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import java.time.Instant;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(properties = "spring.cloud.config.enabled=false")
@AutoConfigureMockMvc
@ActiveProfiles("test")
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
class PatientRestControllerTest {

    @Autowired
    private MockMvc mockMvc;

    private static final JacksonConfiguration jacksonConfiguration = new JacksonConfiguration();

    @Test
    @WithMockUser(username = "user", password = "user", authorities = "ROLE_USER")
    void getPatientById() throws Exception {
        mockMvc.perform(get("/api/v1/patient/{patientId}", 0)
                        .accept(MediaType.APPLICATION_JSON).with(csrf()))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().json(UTHelper.readFileAsString("expected/controller/patientRobinson.json")));
    }

    @Test
    @WithMockUser(username = "user", password = "user", authorities = "ROLE_USER")
    void getPatientByFirstAndLastName() throws Exception {
        String firstName = "Thomas";
        String lastName = "Robinson";
        mockMvc.perform(get("/api/v1/patient")
                        .param("firstName", firstName)
                        .param("lastName", lastName)
                        .accept(MediaType.APPLICATION_JSON).with(csrf()))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().json(UTHelper.readFileAsString("expected/controller/patientRobinson.json")));
    }

    @Test
    @WithMockUser(username = "user", password = "user", authorities = "ROLE_USER")
    void givenPatientSent_whenAddPatient_returnAddedPatient() throws Exception {
        //Given
        PatientDTO patientDTO = new PatientDTO();
        patientDTO.setFirstName("Robert");
        patientDTO.setLastName("Patraison");
        patientDTO.setAddress("425 Sunlight Bd.");
        patientDTO.setGender(PatientDTO.GenderEnum.MALE);
        patientDTO.setBirthDate(Instant.parse("1998-01-19T00:00:00Z"));
        patientDTO.setPhoneNumber("008-454-1552");
        //When-Then
        mockMvc.perform(post("/api/v1/patient")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(patientDTO))
                        .accept(MediaType.APPLICATION_JSON)
                        .with(csrf()))
                .andExpect(status().isOk())
                .andExpect(content().json(UTHelper.readFileAsString("expected/controller/addPatient.json")));
    }

    @Test
    @WithMockUser(username = "user", password = "user", authorities = "ROLE_USER")
    void givenPatientSent_whenUpdatePatient_returnAddedPatient() throws Exception {
        //Given
        PatientDTO patientDtoUpdated = new PatientDTO();
        patientDtoUpdated.setFirstName("Thomas");
        patientDtoUpdated.setLastName("Robinson");
        patientDtoUpdated.setAddress("214 Roger Island St.");
        patientDtoUpdated.setGender(PatientDTO.GenderEnum.FEMALE);
        patientDtoUpdated.setBirthDate(Instant.parse("1995-02-09T00:00:00Z"));
        patientDtoUpdated.setPhoneNumber("066-254-1452");
        patientDtoUpdated.setId(0);
        //When-Then
        mockMvc.perform(put("/api/v1/patient")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(patientDtoUpdated))
                        .accept(MediaType.APPLICATION_JSON)
                        .with(csrf()))
                .andExpect(status().isOk())
                .andExpect(content().json(UTHelper.readFileAsString("expected/controller/patientRobinsonUpdated.json")));
    }

    /**
     * As json string string.
     *
     * @param obj the obj
     * @return the string
     */
    public static String asJsonString(final Object obj) {
        try {
            final ObjectMapper mapper = jacksonConfiguration.objectMapper();
            return mapper.writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

}