package com.abernathyclinic.mediscreen.repositories;

import com.abernathyclinic.mediscreen.entities.EDiabetesAssessment;
import com.abernathyclinic.mediscreen.entities.Gender;
import com.abernathyclinic.mediscreen.entities.Patient;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ActiveProfiles;

import java.time.Instant;

import static org.assertj.core.api.Assertions.assertThat;

@ActiveProfiles("test")
@DataJpaTest
class IPatientRepositoryTest {

    @Autowired
    IPatientRepository IPatientRepository;

    Patient patient;


    @BeforeEach
    public void prepare() {
        patient = new Patient(1, "thomas", "Robinson", Instant.parse("1997-12-03T00:00:00Z"), Gender.MALE, "Rue de crusoë, 75001 PARIS", "06.69.84.82.23", EDiabetesAssessment.NONE);
    }

    @Test
    void givenCreatedPatient_whenSavePatient_shouldReturnPatientOK() {
        //WHEN
        Patient actualPatient = IPatientRepository.save(patient);

        //THEN
        assertThat(actualPatient.getFirstName()).isEqualTo(patient.getFirstName());
        assertThat(actualPatient.getLastName()).isEqualTo(patient.getLastName());
        assertThat(actualPatient.getGender()).isEqualTo(Gender.MALE);
        assertThat(actualPatient.getDiabetesAssessment()).isEqualTo(EDiabetesAssessment.NONE);
        assertThat(actualPatient.getAddress()).isEqualTo(patient.getAddress());
        assertThat(actualPatient.getPhoneNumber()).isEqualTo(patient.getPhoneNumber());
        assertThat(actualPatient.getId()).isEqualTo(1);
    }
}
