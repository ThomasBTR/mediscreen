package com.abernathyclinic.mediscreen;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import static org.assertj.core.api.Assertions.assertThat;

@ActiveProfiles("test")
@SpringBootTest(properties = "spring.cloud.config.enabled=false")
class MediscreenApplicationTests {

    @Test
    void contextLoads() {
        assertThat(true).isTrue();
    }

}
