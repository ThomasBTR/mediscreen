package com.abernathyclinic.mediscreen.services;

import com.abernathyclinic.mediscreen.entities.EDiabetesAssessment;
import com.abernathyclinic.mediscreen.entities.Gender;
import com.abernathyclinic.mediscreen.entities.Patient;
import com.abernathyclinic.mediscreen.models.PatientDTO;
import com.abernathyclinic.mediscreen.repositories.IPatientRepository;
import com.abernathyclinic.mediscreen.utils.IPatientToDtoMapper;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;

import java.time.Instant;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
@SpringBootTest(properties = "spring.cloud.config.enabled=false")
@ActiveProfiles("test")
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_CLASS)
class PatientServiceTest {

    @Autowired
    PatientService patientServiceUnderTest;

    @MockBean
    IPatientRepository patientRepository;

    Patient patient;

    Patient patientUpdate;

    @BeforeEach
    void setUp() {
        patient = new Patient(1, "thomas", "Robinson", Instant.parse("1997-12-03T00:00:00Z"), Gender.valueOf("MALE"), "Rue de crusoë, 75001 PARIS",
                "06.69.84.82.23", EDiabetesAssessment.NONE);
        patientUpdate = new Patient(1, "thomas", "Robinson", Instant.parse("2000-12-03T00:00:00Z"), Gender.valueOf("FEMALE"), "Rue de crusoë, 75001 PARIS",
                "06.69.84.82.23", EDiabetesAssessment.NONE);

    }

    @AfterEach
    void tearDown() {
        patientRepository.deleteAll();
    }

    @Test
    void giventPatientInDb_whenCallingGetPatient_shouldReturnPatient() {
        //Given
        when(patientRepository.getReferenceById(1)).thenReturn(patient);
        //When
        PatientDTO actualPatient = patientServiceUnderTest.getPatient(1);
        PatientDTO expectedPatient = IPatientToDtoMapper.INSTANCE.patientToDTO(patient);

        //Then
        assertThat(actualPatient).isEqualTo(expectedPatient);
    }

    @Test
    void givenPatientSent_shouldReturnPatientSaved() {
        //Given
        PatientDTO alberthoDTO = new PatientDTO();
        alberthoDTO.setGender(PatientDTO.GenderEnum.MALE);
        alberthoDTO.setLastName("Funky");
        alberthoDTO.setFirstName("Albertho");
        alberthoDTO.setPhoneNumber("001-123-4657");
        alberthoDTO.setBirthDate(Instant.parse("1998-02-19T00:00:00Z"));
        Patient albertho = IPatientToDtoMapper.INSTANCE.patientDTOtoPatient(alberthoDTO);
        when(patientRepository.getPatientByFirstNameAndLastName("Albertho","Funky")).thenReturn(albertho);
        //When
        PatientDTO actualPatientDTO = patientServiceUnderTest.getPatient("Albertho","Funky");
        //Then
        assertThat(actualPatientDTO).isEqualTo(alberthoDTO);
    }

    @Test
    void givenPatient_whenCallingAddPatient_shouldSaveAndReturnPatient() {
        //Given
        when(patientRepository.save(any(Patient.class))).thenReturn(patient);
        PatientDTO expectedAddedPatient = IPatientToDtoMapper.INSTANCE.patientToDTO(patient);
        //When
        PatientDTO actualAddedPatient = patientServiceUnderTest.addPatient(expectedAddedPatient);
        //Then
        assertThat(actualAddedPatient).isEqualTo(expectedAddedPatient);
    }

    @Test
    void giventPatient_whenCallingUpdatePatient_shouldSaveAndReturnPatient() {
        //Given
        when(patientRepository.save(any(Patient.class))).thenReturn(patientUpdate);
        PatientDTO expectedUpdatedPatient = IPatientToDtoMapper.INSTANCE.patientToDTO(patientUpdate);
        //When
        PatientDTO actualUpdatedPatient = patientServiceUnderTest.updatePatient(expectedUpdatedPatient);
        assertThat(actualUpdatedPatient).isEqualTo(expectedUpdatedPatient);
    }
}