<img src="https://img.shields.io/badge/java-%23ED8B00.svg?&style=for-the-badge&logo=java&logoColor=white"/> * * *  <img src="https://img.shields.io/badge/spring%20-%236DB33F.svg?&style=for-the-badge&logo=spring&logoColor=white"/> 

# Mediscreen-App


## Getting Started

run the project with a command : ``mvn spring-boot:run``

Get tests results through : ``mvn clean verify``

Install the jar-with-dependencies with :  ``mvn clean install``

Get the docker image : After running the previous command, in root folder, run ``docker build -t mediscreen-1.0.0 .``

Running the docker image : ``docker run -p 9001:9001 mediscreen-app:1.0.0``

You can build the image docker to your local Docker damon using:
``mvn compile jib:dockerBuild``

## Technical:

1. Java 17
2. Framework: Spring Boot v2.7.5
3. Thymeleaf
4. Bootstrap
5. Current version : 3.0.0

### Running App

You can run the application in two different ways:

1. Run it from docker-compose available on [mediscreen-module](https://gitlab.com/mediscreen-apps/mediscreen-modules)
Don't forget to specify witch image (corresponding to the version) you want to run
___ On the two following method, you will need to run the [config-server](https://gitlab.com/mediscreen-apps/config-server) first.
2. import the code into an IDE of your choice and run the Application.java to launch the application.
3. Or import the code, unzip it, open the console, go to the root folder that contains the gradlew file, then execute
the below command to launch the application.

```bash
mvn spring-boot:run -Dspring-boot.run.jvmArguments="-Dspring.cloud.config.uri=http://localhost:9101"
```



## Projet setup :
1. Following data is available on folders :
    - Source root: src/main/java
    - View: src/main/resources/templates
    - Static: src/main/resource/static
2. application.properties contains all the properties regarding spring, database and more
3. Database is available in src/main/resources/database with 2 files : schema.sql for database init and data.sql to
   populate the database
4. openapi.yaml is available with all the Restfull API path

## Git Repo

The git repo is [here](https://gitlab.com/mediscreen-apps/mediscreen-app)

All the branches are up even though all of them have been merged on the main branch with the release version.

### Testing

The app has unit tests written.

To run the tests from maven, go to the folder that contains the pom.xml file and execute the below command.

```bash
mvn clean verify
```
### Coverage

```bash
mvn clean verify
```
The tests coverage can be accessed after building the app thanks to Jacoco in the target/site/index.html or jacoco.xml


## Security

1. The app support OAuth2 authentication with GitHub and Google
2. 2 users are available on the database :
    - (U$er2022) is the password for user
    - (@dMin2022) is the password for admin
3. Each password requires :
    - At least 8 characters,
    - At least 1 lower case character
    - At least 1 Upper case character
    - At least 1 Special character
4. All the password are encrypted before being saved in the database
5. No decryption is available through the RestFull API (password will be returned encrypted) to ensure protection.